#! /bin/bash
#
# Build the Docker image for a series of different Forge versions
#
set -e

# MC version list
readonly -a mcversions=(
	"1.12.2"
	"1.16.5"
	"1.18.2"
)

# Forge version dictionary (we only support one version per)
readonly -A forgeversions=(
	["1.12.2"]="14.23.5.2855"
	["1.16.5"]="36.2.35"
	["1.18.2"]="40.1.84"
)

# JRE versions (in the form of Alpine packages)
readonly -A jreversions=(
	["1.12.2"]="openjdk8-jre"
	["1.16.5"]="openjdk11-jre"
	["1.18.2"]="openjdk17-jre"
)

# Build images
docker buildx create --use
for mc in ${mcversions[@]}; do
	forge="${forgeversions[$mc]}"
	jre="${jreversions[$mc]}"
	CI_HUB_USERNAME="${CI_HUB_USERNAME:=rehashedsalt}"
	CI_PROJECT_NAME="${CI_PROJECT_NAME:=minecraft-forge}"
	tag="$CI_HUB_USERNAME/$CI_PROJECT_NAME:$mc-${CI_COMMIT_REF_NAME:=bleeding}"
	echo "Building image..."
	echo "    Minecraft:   $mc"
	echo "    Forge:       $forge"
	echo "    JRE:         $jre"
	echo "    Ref:         $CI_COMMIT_REF_NAME"
	echo "Dockerhub tag:   $tag"
	# --no-cache is required for clean builds
	docker buildx build \
		--build-arg MINECRAFT_VERSION="$mc" \
		--build-arg FORGE_VERSION="$forge" \
		--build-arg JRE_VERSION="$jre" \
		--no-cache \
		--platform linux/amd64 \
		--progress plain \
		--tag "$tag" \
		--push \
		.
done
docker images
