#! /bin/bash
#
# start-server.sh
# Copyright (C) 2021 Vintage Salt <rehashedsalt@cock.li>
#
# Distributed under terms of the MIT license.
#
set -e

# Download and extract a pack zip, if one is provided
if [ -n "$FORGE_PACK_ZIP" ]; then
	echo "Downloading pack: $FORGE_PACK_ZIP"
	tmpdir="$(mktemp -d)"
	pushd "$tmpdir" > /dev/null 2>&1
	curl -L "$FORGE_PACK_ZIP" -o pack.zip
	unzip -q pack.zip
	ls -alh
	directory="$(find . -type d -iname "mods" -execdir pwd \; | sort -n | head -n 1)"
	if [ -z "$directory" ]; then
		echo "Unable to find mods directory"
	else
		echo "Found modpack directory: $directory"
	fi
	echo "Syncing content to /minecraft"
	rsync -a --no-perms --no-owner --no-group --ignore-existing "$directory"/ /minecraft/
	popd > /dev/null 2>&1
fi

# Then also download and extract a config repo, if one exists
if [ -n "$CONFIG_REPO" ]; then
	echo "Downloading config repo: $CONFIG_REPO"
	tmpdir="$(mktemp -d)"
	pushd "$tmpdir" > /dev/null 2>&1
	git clone "$CONFIG_REPO" .
	rm -rf .git
	rsync -av --delete ./ /minecraft/
	popd > /dev/null 2>&1
fi

# Memory configuration
[ -n "$JRE_XMX" ]	&& args="-Xmx$JRE_XMX $args"
[ -n "$JRE_XMS" ]	&& args="-Xms$JRE_XMS $args"
[ -n "$ARGS" ]		&& args="$ARGS $args"

# For versions >=1.18, we need to obey some more complex requirements
if [ -e "run.sh" ]; then
	prog="java"
	for file in libraries/net/minecraftforge/forge/*/unix*args*txt; do
		args="$args @$file"
	done
else
	# For all other versions, manually calling the server jar will be fine
	prog="java"
	args="$args -jar server.jar nogui"
fi

# Debugging info
java -version
echo "Invoking: $prog $args"
echo
echo "To see the server console, execute this command in the container:"
echo "  screen -r minecraft"

# Start 'er up
cleanup() {
	screen -p 0 -S minecraft -X stuff save-all^M
	screen -p 0 -S minecraft -X stuff stop^M
}
trap cleanup EXIT
screen -DmS minecraft $prog $args

