# docker-minecraft

A Dockerfile and some assorted scripts to build a Minecraft Forge server container.

## Usage

Deploy as expected:

```bash
docker run -p 25565:25565 rehashedsalt/minecraft-forge:1.16.5-master
```

And badda bing, badda boom, you'll have a Minecraft Forge server of whatever the tag is.

You can see what the supported combinations of tags are by looking them up in `build.sh`. Each version only supports one edition of Java and one edition of Forge. Getting CI/CD to automagically determine the latest version of Forge is on the to-do list.

Admittedly, I don't use tags very often. You'd do well to target `master` as the refname if you want bleeding edge stuff.

Full list of tags is available on [Dockerhub](https://hub.docker.com/r/rehashedsalt/minecraft-forge/tags).

## Server Console

You can access the server console in a screen session pretty easily:

```bash
docker exec -it mycontainer screen -r minecraft
```

## Application State

For obvious reasons, you'll want to consult the documentation of any mods you have installed, but a majority of application state lives here-ish:

* `/minecraft/world`

* `/minecraft/ops.json`, `whitelist.json`, `banned-ips.json`, `banned-players.json`, etc.

* `/minecraft/logs`

## Environment Variables

variable|description
---|---
`JRE_XMX`|Maximum amount of heap passed to the main Minecraft process
`JRE_XMS`|Minimum heap size passed to the main Minecraft process
`FORGE_PACK_ZIP`|If provided, the URL to a zip or tar.gz file that contains the modpack that needs to be installed. Will be intelligently extracted into the server directory through the magic of `find`.
`CONFIG_REPO`|If provided, the URI to a git repository that contains configuration to copy over top the pack. The root of the repo will be the root of the server directory.
`ARGS`|Any additional arguments to be passed to the JVM

## Useful Arguments

The following arguments may be useful to pass through `$ARGS` from time to time:

* `-Dfml.queryResult=confirm` - Useful when confirming through world corruption

* Consider [this advice from a Forge dev](https://old.reddit.com/r/feedthebeast/comments/5jhuk9/modded_mc_and_memory_usage_a_history_with_a/) (circa 5+ years ago but still pretty relevant)

## Weird Defaults

The default `server.properties` contains the following changes to better facilitate usage in a modded environment:

```
allow-flight=true
difficulty=hard
enable-command-block=true
spawn-protection=0
```

## FAQ

**Why do you download the pack at runtime?**

It allows for transparent upgrades without having to build a whole new container. You may lose container consistency, but you gain simplicity in definition.

Of course, nothing prevents you from just not using this magic variable and instead `FROM`ing this container and building your own pack.

## License

MIT
