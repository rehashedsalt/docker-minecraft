# Args
ARG MINECRAFT_VERSION="1.16.5"
ARG FORGE_VERSION="36.2.26"
ARG JRE_VERSION="openjdk8-jre"

ARG UID=1520
ARG USER=minecraft
ARG GID=1520
ARG GROUP=minecraft

# The first stage just builds up the modpack
FROM alpine:latest AS build

# Use all of our arguments
ARG MINECRAFT_VERSION
ARG FORGE_VERSION
ARG JRE_VERSION

# Build us up the basics of the Minecraft server environment
COPY start-server.sh /minecraft/start-server.sh
WORKDIR /minecraft
RUN apk add curl "${JRE_VERSION}" &&\
	curl -L "https://files.minecraftforge.net/maven/net/minecraftforge/forge/${MINECRAFT_VERSION}-${FORGE_VERSION}/forge-${MINECRAFT_VERSION}-${FORGE_VERSION}-installer.jar" -o installer.jar &&\
	java -jar installer.jar --installServer &&\
	echo "eula=true" > eula.txt &&\
	rm installer.jar installer.jar.log && \
	chmod +x *.sh && \
	ln -s forge-*.jar server.jar

# Stuff them in a smaller container with fewer layers
FROM alpine:latest AS final

# Use only a subset of arguments
ARG JRE_VERSION
ARG UID
ARG USER
ARG GID
ARG GROUP

# Build the thing up
RUN apk add bash curl findutils rsync screen "${JRE_VERSION}"
WORKDIR /minecraft
COPY --from=build /minecraft .
RUN addgroup -g "${GID}" "${GROUP}" && \
	adduser -h /minecraft -s /bin/sh -D -H -u "${UID}" -G "${GROUP}" "${USER}" && \
	chown "${USER}:${GROUP}" /minecraft
USER $USER
CMD [ "bash", "start-server.sh" ]
EXPOSE 25565
